﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssignmentForUptime.Models
{
    public class AmazonProductItem
    {
        public string ImageUrl { get; set; }
        public string Title { get; set; }
        public string PageUrl { get; set; }
        public double Price { get; set; }
        public string Currency { get; set; }
    }
}
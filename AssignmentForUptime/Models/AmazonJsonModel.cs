﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssignmentForUptime.Models
{
    public class AmazonJsonModel
    {
        public List<AmazonProductItem> AmazonItems { get; set; }
        public string Error { get; set; }
    }
}
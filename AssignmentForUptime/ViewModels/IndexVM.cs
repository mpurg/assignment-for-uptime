﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssignmentForUptime.ViewModels
{
    public class IndexVM
    {
        public Dictionary<string,string> CurrencyList { get; set; }
        public String SelectedElement { get; set; }
        public IndexVM()
        {
        }
        public IndexVM(Dictionary<string, string> currencyList, string id)
        {
            this.CurrencyList = currencyList;
            this.SelectedElement = id;
        }

    }
}
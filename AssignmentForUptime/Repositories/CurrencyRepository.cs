﻿using AssignmentForUptime.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Linq;

namespace AssignmentForUptime.Repositories
{
    public class CurrencyRepository
    {
        private const string CURRENCY_LOOKUP_URL = "https://openexchangerates.org/api/currencies.json";
        private const string CURRENCY_Rate_URL = "https://openexchangerates.org/api/latest.json";
        private const string APP_ID_SUFFIX = "?app_id=";
        private const string APP_ID = "your open exchange rates app id";
        public static Dictionary<string, string> getCurrencies()
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            JObject jObject = JObject.Parse(loadJsonData(CURRENCY_LOOKUP_URL));
            foreach (JToken item in jObject.Children())
            {
                result[item.Value<JProperty>().Name] = item.Value<JProperty>().Value.ToString();
            }
            result = result.OrderBy(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
            return result;
        }

        public static double calculateCurrencyRate(string from, string to)
        {
            if (from.Equals(to))
            {
                return 1d;
            }
            string url = CURRENCY_Rate_URL + APP_ID_SUFFIX + APP_ID;
            double result = 0d;
            JObject wholeJson = JObject.Parse(loadJsonData(url));
            JToken rates = wholeJson["rates"];
            string fromCurrency = rates[from].ToString();
            double fromRate;
            if (!Double.TryParse(fromCurrency, out fromRate))
            {
                return 1d;
            }
            string toCurrency = rates[to].ToString();
            double toRate;
            if (!Double.TryParse(toCurrency, out toRate))
            {
                return 1d;
            }
            result = (1 / fromRate) * toRate;
            return result;
        }

        public static string loadJsonData(string url)
        {
            string json;
            using (WebClient wc = new WebClient())
            {
                json = wc.DownloadString(url);
            }
            return json;
        }
    }
}
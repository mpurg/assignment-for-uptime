﻿using AssignmentForUptime.Helpers;
using AssignmentForUptime.Models;
using System;
using System.Collections.Generic;
using System.Web;
using System.Xml;
using System.Linq;

namespace AssignmentForUptime.Repositories
{
    public class AmazonRepository
    {
        private const string AWS_TAG = "Your aws tag";
        private const string AWS_ACCESS_KEY_ID = "your aws access key id";
        private const string AWS_SECRET_KEY = "your aws secret key";
        private const string DESTINATION = "webservices.amazon.co.uk";//can be other servers, I only used the uk one.
        private const int PAGE_LIMIT = 5;
        private const int ITEMS_PER_PAGE_LIMIT = 10;
        private const int ITEMS_PER_PAGE = 13;
        public AmazonJsonModel getItems(string keyword, int page, string currency)
        {
            AmazonJsonModel result = new AmazonJsonModel();
            result.AmazonItems = new List<AmazonProductItem>();
            if (page < 1)
            {
                return result;
            }
            else if (!thereAreNewProductsAvailable(page))
            {
                return result;
            }

            int firstElement = ITEMS_PER_PAGE * (page - 1) + 1;
            int nextPage = firstElement / ITEMS_PER_PAGE_LIMIT + 1;
            int removeCount = (firstElement - 1) % ITEMS_PER_PAGE_LIMIT;
            int elementCount = 0;

            for (; nextPage <= PAGE_LIMIT; nextPage++)
            {
                if (elementCount - removeCount >= ITEMS_PER_PAGE)
                {
                    break;
                }
                String query = getQueryUrl(keyword, nextPage);
                XmlDocument xDoc = loadData(query);
                result.Error = loadErrors(xDoc);
                if (result.Error != "")
                {
                    break;
                }
                result.AmazonItems.AddRange(parseData(xDoc));
                elementCount += ITEMS_PER_PAGE_LIMIT;
            }
            if (result.Error != "")
            {
                return result;
            }
            //removing unnecessary elements.
            for (; removeCount > 0; removeCount--)
            {
                result.AmazonItems.RemoveAt(0);
            }
            int numberOfElements = result.AmazonItems.Count - ITEMS_PER_PAGE;
            if (numberOfElements > 0)
            {
                result.AmazonItems.RemoveRange(ITEMS_PER_PAGE, numberOfElements);
            }

            result.AmazonItems = calculateToCurrency(result.AmazonItems, currency);
            return result;
        }

        private List<AmazonProductItem> calculateToCurrency(List<AmazonProductItem> list, string toCurrency)
        {
            if (list == null)
            {
                return null;
            }
            string fromCurrency = list.First().Currency;
            double rate = CurrencyRepository.calculateCurrencyRate(fromCurrency, toCurrency);
            if (rate.Equals(1d))
            {
                foreach (AmazonProductItem item in list)
                {
                    double price = item.Price /100;
                    item.Price = Math.Round(price, 2, MidpointRounding.AwayFromZero);
                }
            }
            else
            {
                foreach (AmazonProductItem item in list)
                {
                    double price = item.Price * rate;
                    price = price / 100;
                    item.Price = Math.Round(price, 2, MidpointRounding.AwayFromZero);
                }
            }

            return list;
        }

        private bool thereAreNewProductsAvailable(int page)
        {
            if ((page - 1) * ITEMS_PER_PAGE >= PAGE_LIMIT * ITEMS_PER_PAGE_LIMIT)
            {
                return false;
            }
            return true;
        }

        private string getQueryUrl(string keyword, int pageNumber)
        {
            IDictionary<string, string> requestDictionary = new Dictionary<string, string>();
            requestDictionary["Service"] = "AWSECommerceService";
            requestDictionary["Operation"] = "ItemSearch";
            requestDictionary["AWSAccessKeyId"] = AWS_ACCESS_KEY_ID;
            requestDictionary["AssociateTag"] = AWS_TAG;
            requestDictionary["SearchIndex"] = "All";
            requestDictionary["ResponseGroup"] = "Images,ItemAttributes,Offers";
            requestDictionary["Keywords"] = keyword;
            requestDictionary["ItemPage"] = pageNumber.ToString();
            SignedRequestHelper helper = new SignedRequestHelper(AWS_ACCESS_KEY_ID, AWS_SECRET_KEY, DESTINATION);
            return helper.Sign(requestDictionary);
        }

        private List<AmazonProductItem> parseData(XmlDocument xdoc)
        {
            XmlNodeList nodeList = xdoc.GetElementsByTagName("Item");
            List<AmazonProductItem> list = new List<AmazonProductItem>();
            if (nodeList.Count < 1)
            {
                return list;
            }
            foreach (XmlNode item in nodeList)
            {
                AmazonProductItem product = new AmazonProductItem();

                product.PageUrl = getTextFromNode(item, "DetailPageURL");

                XmlNode node = findNode(item, "ItemAttributes");
                product.Title = getTextFromNode(node, "Title");

                node = findNode(item, "OfferSummary");
                if (node != null)
                {
                    node = findNode(node, "LowestNewPrice");
                    if (node != null)
                    {
                        product.Price = double.Parse(getTextFromNode(node, "Amount"));
                        product.Currency = getTextFromNode(node, "CurrencyCode");
                    }
                }

                node = findNode(item, "MediumImage");
                if (node != null)
                {
                    product.ImageUrl = getTextFromNode(node, "URL");
                }

                list.Add(product);
            }
            return list;
        }
        public string loadErrors(XmlDocument xdoc)
        {
            XmlNodeList list = xdoc.GetElementsByTagName("Error");
            if (list.Count > 0)
            {
                XmlNode node = list.Item(0);
                string error = getTextFromNode(node, "Code");
                error += ", " + getTextFromNode(node, "Message");
                return error;
            }
            return "";
        }
        private XmlNode findNode(XmlNode list, string nodeName)
        {
            if (list.HasChildNodes)
            {
                foreach (XmlNode node in list.ChildNodes)
                {
                    if (node.Name.Equals(nodeName)) return node;
                }
            }
            return null;
        }

        private string getTextFromNode(XmlNode node, string nodeName)
        {
            if (node.HasChildNodes)
            {
                foreach (XmlNode innerNode in node.ChildNodes)
                {
                    if (innerNode.Name.Equals(nodeName)) return innerNode.InnerText;
                }
            }
            return "";
        }

        private XmlDocument loadData(string query)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(query);
            return doc;
        }
    }
}
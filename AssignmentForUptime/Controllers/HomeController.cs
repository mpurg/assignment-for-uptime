﻿using AssignmentForUptime.Repositories;
using AssignmentForUptime.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AssignmentForUptime.Controllers
{
    public class HomeController : Controller
    {
        AmazonRepository repo;
        public HomeController()
        {
            repo = new AmazonRepository();
        }
        public ActionResult Index()
        {
            return View(new IndexVM(CurrencyRepository.getCurrencies(), "EUR"));
        }
        public ActionResult Redirect()
        {
            return Redirect(Url.Action("Index", "Home"));
        }
        [HttpPost]
        public ActionResult GetProducts(string input, int page, string currency)
        {
            return Json(repo.getItems(input, page, currency));
        }
    }
}
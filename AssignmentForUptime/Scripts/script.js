﻿var timeout = null;
var currency = null;
var page = 1;
var input = null;
var currentData = [];
var nextData = [];
var prevData = [];

$("#products").keyup(function () {
    clearTimeout(timeout);
    timeout = setTimeout(function () {
        if ($("#products").val().length < 3) {
            return;
        }
        input = $("#products").val();
        page = 1;
        changeCurrency();
        searchForNewProducts();
    }, 500);
})

function searchForNewProducts() {
    overlay();
    getData(page, true,function () {
        loadData(currentData);
        deleteOverlay();
    });
    preloadNeighbourData();
}

function preloadNeighbourData() {
    $("#footer").empty();
    getData(page + 1, false, function () {
        nextData = currentData;
        setNextNavigation();
    });
    getData(page - 1, false, function () {
        prevData = currentData;
        setPrevNavigation();
    });
}

function setNextNavigation() {
    if (nextData.length > 0) {
        button = $("<button>").attr({ "onclick": "next()", "id": "nextBtn" }).html(">");
        $("#footer").append(button);
    }
}

function setPrevNavigation() {
    if (prevData.length > 0) {
        var button = $("<button>").attr({ "onclick": "prev()", "id": "prevBtn" }).html("<");
        $("#footer").append(button);
    }
}

function getData(page, showError ,callback) {
    $.post("Home/GetProducts",
           { "input": input, "page": page, "currency": currency },
           function (data) {
               currentData = [];
               if (data.Error && showError) {
                   alert(data.Error);
                   callback();
                   return;
               }
               if (data.AmazonItems.length < 1) {
                   callback();
                   return;
               }
               $.each(data.AmazonItems, function () {
                   var model = {
                       ImageUrl: this.ImageUrl,
                       PageUrl: this.PageUrl,
                       Price: this.Price,
                       Title: this.Title,
                       Currency: this.Currency
                   }
                   currentData.push(model);
               });
               callback();

           }, "json");
}

function loadData(data) {
    if (data.length < 1) {
        return;
    }
    $("body").scrollTop(0);
    var place = $("#content");
    place.fadeOut("fast", function () {

        place.empty();
        $.each(data, function () {
            var container = $("<div>").attr({
                "onclick": "window.location = '" + this.PageUrl + "'"
            }).addClass("container");

            var image = $("<img>")
                .attr({
                    "src": this.ImageUrl,
                    "alt": "Picture not available"
                }).on('load', function () { $(this).fadeIn("fast"); });
            container.append(image);
            var titleContainer = $("<div>").addClass("titleContainer");
            var title = $("<p>").html(this.Title).addClass("title");
            titleContainer.append(title);
            container.append(titleContainer);

            var price = $("<p>").html(this.Price + " " + currency).addClass("price");
            container.append(price);

            place.append(container);

        });
        place.fadeIn("fast");

    });
}

function next() {
    page++;
    loadData(nextData);
    preloadNeighbourData();
}

function prev() {
    page--;
    loadData(prevData);
    preloadNeighbourData();
}

function changeCurrency() {
    var e = document.getElementById("currencyList");
    currency = e.options[e.selectedIndex].value;
}

$("#currencyList").change(function () {
    if (input == null) {
        return;
    }
    changeCurrency();
    searchForNewProducts();
});

function overlay() {
    var place = $("#overlay");
    var div = $("<div>").addClass("overlay");
    var img = $("<img>").attr("src", "../loading.gif");
    div.append(img);
    place.append(div);
}
function deleteOverlay() {
    $("#overlay").empty();
}